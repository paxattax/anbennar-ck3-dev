#k_tretun
##d_tretun
###c_tretun
48 = {		#Tretun

    # Misc
    culture = tretunic 
    religion = cult_of_the_dame
    holding = castle_holding

    # History
}
951 = {

    # Misc
	holding = none

    # History

}
1001 = {

    # Misc
	holding = city_holding

    # History

}

###c_lanpool
47 = {		#Lanpool

    # Misc
    culture = tretunic
    religion = cult_of_the_dame
    holding = castle_holding

    # History
}
950 = {

    # Misc
	holding = none

    # History

}

##d_roilsard
###c_roilsard
59 = {		#Roilsard

    # Misc
    culture = roilsardi
    religion = court_of_adean
    holding = castle_holding

    # History
}
1007 = {

    # Misc
	holding = castle_holding

    # History

}
1008 = {

    # Misc
	holding = none

    # History

}
1009 = {

    # Misc
	holding = city_holding

    # History

}

###c_saloren
32 = {		#Saloren

    # Misc
    culture = roilsardi
    religion = house_of_minara
    holding = castle_holding

    # History
}
1094 = {

    # Misc
	holding = none

    # History

}
33 = {		#Toarnaire

    # Misc
    holding = city_holding

    # History
}

###c_loopuis
34 = {		#Loopuis

    # Misc
    culture = roilsardi
    religion = house_of_minara
    holding = castle_holding

    # History
}
1005 = {

    # Misc
	holding = church_holding

    # History

}
1006 = {

    # Misc
	holding = none

    # History

}

###c_vivinmar
40 = {		#Tedertoar

    # Misc
    culture = roilsardi
    religion = court_of_adean
    holding = castle_holding

    # History

}
1003 = {

    # Misc
	holding = none

    # History

}
1002 = {

    # Misc
	holding = castle_holding

    # History

}
1004 = {

    # Misc
	holding = city_holding

    # History

}
