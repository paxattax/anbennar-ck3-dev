﻿blue_reachman = {
	color = { 80 107 232 }
	created = 700.1.1
	parents = { dalric old_alenic }
	
	ethos = ethos_stoic
	heritage = heritage_alenic
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_parochialism
		tradition_stalwart_defenders
	}
	
	name_list = name_list_alenic
	
	coa_gfx = { frisian_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		25 = caucasian_blond
		5 = caucasian_ginger
		10 = caucasian_brown_hair
		60 = caucasian_dark_hair
	}
}

old_alenic = {
	color = { 82  130  152 }
	
	ethos = ethos_stoic
	heritage = heritage_alenic
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hunters
		tradition_forest_folk
		tradition_forest_fighters
	}
	
	name_list = name_list_old_alenic
	
	coa_gfx = { frisian_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		30 = caucasian_blond
		10 = caucasian_brown_hair
		60 = caucasian_dark_hair
	}
}

gawedi = {
	color = "gawedi_blue"
	created = 400.1.1
	parents = { old_alenic }
	
	ethos = ethos_stoic
	heritage = heritage_alenic
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_pastoralists
		tradition_strength_in_numbers
		tradition_martial_admiration
		tradition_forest_fighters
	}
	
	name_list = name_list_alenic
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		15 = caucasian_blond
		10 = caucasian_brown_hair
		75 = caucasian_dark_hair
	}
}

nuralenic = {
	color = "gawedi_blue"
	created = 1100.1.1
	parents = { gawedi moon_elvish }
	
	ethos = ethos_courtly
	heritage = heritage_alenic
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
	}
	
	name_list = name_list_alenic
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond
		30 = caucasian_brown_hair
		50 = caucasian_dark_hair
	}
}

moorman = {
	color = { 71  84  102 }
	created = 470.8.1	#actual canon date according to dragonwake
	parents = { old_alenic }
	
	ethos = ethos_communal
	heritage = heritage_alenic
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_wetlanders
		tradition_staunch_traditionalists
		tradition_collective_lands
	}
	
	name_list = name_list_moorman
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond
		10 = caucasian_brown_hair
		70 = caucasian_dark_hair
	}
}

vertesker = {
	color = { 57  81  87 }
	created = 800.1.1	
	parents = { gawedi crownsman }
	
	ethos = ethos_bureaucratic
	heritage = heritage_alenic
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_wetlanders
		tradition_city_keepers
		tradition_maritime_mercantilism
	}
	
	name_list = name_list_alenic
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		5 = caucasian_blond
		15 = caucasian_brown_hair
		80 = caucasian_dark_hair
	}
}

wexonard = {
	color = { 97  0  137 }
	created = 474.7.1	#canon
	parents = { old_alenic }
	
	ethos = ethos_bellicose
	heritage = heritage_alenic
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_fp1_trials_by_combat
		tradition_hard_working
		tradition_spartan
		tradition_ruling_caste
	}
	
	name_list = name_list_wexonard
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		50 = caucasian_blond
		25 = caucasian_brown_hair
		25 = caucasian_dark_hair
	}
}

#Elvenized wex
thilosian = {
	color = { 97  0  237 }
	created = 1100.1.1
	parents = { wexonard moon_elvish }
	
	ethos = ethos_courtly
	heritage = heritage_alenic
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
	}
	
	name_list = name_list_wexonard
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		50 = caucasian_blond
		25 = caucasian_brown_hair
		25 = caucasian_dark_hair
	}
}

marrodic = {
	color = { 157 121 115 }
	created = 492.8.1	#canon
	parents = { old_alenic }
	
	ethos = ethos_stoic
	heritage = heritage_alenic
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_isolationist
		tradition_mountain_homes
	}
	
	name_list = name_list_old_alenic
	
	coa_gfx = { english_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		15 = caucasian_blond
		10 = caucasian_brown_hair
		75 = caucasian_dark_hair
	}
}